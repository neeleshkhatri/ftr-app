import { FarewellComponent } from './number-frequency/components/farewell/farewell.component';
import { NumbersFrequencyComponent } from './number-frequency/components/numbers-frequency/numbers-frequency.component';
import { OutputFrequencyComponent } from './number-frequency/components/output-frequency/output-frequency.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: OutputFrequencyComponent },
  { path: 'numbersfrequency', component: NumbersFrequencyComponent },
  { path: 'farewell', component: FarewellComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

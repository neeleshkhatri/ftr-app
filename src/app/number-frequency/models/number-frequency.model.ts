export class NumberFrequencyModel {
  userNumber: string;
  frequency: number;

  constructor(userNumber: string, frequency: number) {
      this.userNumber = userNumber;
      this.frequency = frequency;
  }
}

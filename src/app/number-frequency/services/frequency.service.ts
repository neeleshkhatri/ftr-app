import { Injectable } from '@angular/core';
import { Observable, timer, BehaviorSubject, NEVER } from 'rxjs';
import { switchMap, map, takeWhile } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FrequencyService {
  constructor() {}

  toggle$ = new BehaviorSubject(true);
  frequencyInSeconds = 3;

  public setFrequency(frequencyInSeconds: number): void {
    this.frequencyInSeconds = frequencyInSeconds;
  }

  public getCountDown(): Observable<number> {
    const milliseconds = 1000;
    const interval = milliseconds;
    const time = milliseconds * this.frequencyInSeconds;
    const currentInterval = () => time / interval;
    return this.toggle$.pipe(
      switchMap((running: boolean) => {
        return running ? timer(0, interval) : NEVER;
      }),
      map((t: number) => {
        return currentInterval() - t;
      }),
      takeWhile((t) => t >= 0)
    );
  }
}

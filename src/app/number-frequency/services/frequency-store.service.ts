import { NumberFrequencyModel } from './../models/number-frequency.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FrequencyStoreService {
  private store = {};
  constructor() {}

  public clear(): void {
    this.store = {};
  }

  public addToStore(userNumber: number): void {
    const storeValue = this.store[userNumber];
    if (!storeValue) {
      this.store[userNumber] = 1;
    } else {
      this.store[userNumber] = storeValue + 1;
    }
  }

  public getFromStore(): NumberFrequencyModel[] {
    return Object.entries(this.store)
    .map(([key, value]) => {
          return {userNumber: key, frequency: parseInt(value.toString(), 10) } as NumberFrequencyModel;
    })
    .sort((a, b) => Number(b.frequency) - Number(a.frequency) );
  }

  public getFormatedText(): string {
    let output = '';

    this.getFromStore().map(
      (item: NumberFrequencyModel) =>
        (output += !output
          ? `${item.userNumber.toString()}:${item.frequency.toString()}`
          : `, ${item.userNumber.toString()}:${item.frequency.toString()}`)
    );
    return output;
  }
}

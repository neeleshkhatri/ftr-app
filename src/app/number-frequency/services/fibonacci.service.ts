import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FibonacciService {

  constructor() {
  }

  public isNumberFibonacci(value: number): boolean {
    const thousandFibonacciNumber = 1000;
    if (value > thousandFibonacciNumber) {
      return false;
    }
    return this.firstCheck(value) || this.secondCheck(value);
  }

  public getNthFibonacci(n: number): number {
    return this.getNthFibonacciNumber(n);
  }

  private  getNthFibonacciNumber(n: number = 1, index: number = 1, f1: number = 0, f2: number = 1): number {
    const f3 = f1 + f2;
    if (n === 1) { return 0; }
    if (n === 2 || n === 3) { return 1; }

    if (index < n) {
      return this.getNthFibonacciNumber(n, index + 1, f2, f3);
    } else {
      return f1;
    }
  }

  private firstCheck(value: number): boolean {
    const a = Math.sqrt( 5 * value * value + 4);
    return Number.isInteger(a);

  }
  private secondCheck(value: number): boolean {
    const a = Math.sqrt( 5 * value * value - 4);
    return Number.isInteger(a);
  }
}

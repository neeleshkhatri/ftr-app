import { TestBed } from '@angular/core/testing';

import { FrequencyStoreService } from './frequency-store.service';

describe('FrequencyStoreService', () => {
  let service: FrequencyStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrequencyStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Store should be empty', () => {
    expect(service.getFormatedText()).toEqual('');
    expect(service.getFromStore().length).toEqual(0);
  });

  it('getFormatedText should return sorted data', () => {
    service.addToStore(3);
    expect(service.getFormatedText()).toEqual('3:1');
    service.addToStore(2);
    expect(service.getFormatedText()).toEqual('2:1, 3:1');
    service.addToStore(1);
    expect(service.getFormatedText()).toEqual('1:1, 2:1, 3:1');
    service.addToStore(1);
    expect(service.getFormatedText()).toEqual('1:2, 2:1, 3:1');
    service.addToStore(1);
    expect(service.getFormatedText()).toEqual('1:3, 2:1, 3:1');
    service.addToStore(3);
    service.addToStore(3);
    service.addToStore(3);
    expect(service.getFormatedText()).toEqual('3:4, 1:3, 2:1');
  });

  

});

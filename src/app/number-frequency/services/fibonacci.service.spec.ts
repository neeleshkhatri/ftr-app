import { TestBed } from '@angular/core/testing';

import { FibonacciService } from './fibonacci.service';

describe('FibonacciService', () => {
  let service: FibonacciService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FibonacciService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('isNumberFibonacci() should return true', () => {
    expect(service.isNumberFibonacci(1)).toBeTrue();
    expect(service.isNumberFibonacci(2)).toBeTrue();
    expect(service.isNumberFibonacci(3)).toBeTrue();
    expect(service.isNumberFibonacci(5)).toBeTrue();
    expect(service.isNumberFibonacci(8)).toBeTrue();
    expect(service.isNumberFibonacci(13)).toBeTrue();
    expect(service.isNumberFibonacci(987)).toBeTrue();
  });

  it('isNumberFibonacci() should return false', () => {
    expect(service.isNumberFibonacci(4)).toBeFalse();
    expect(service.isNumberFibonacci(6)).toBeFalse();
    expect(service.isNumberFibonacci(7)).toBeFalse();
    expect(service.isNumberFibonacci(10)).toBeFalse();
    expect(service.isNumberFibonacci(14)).toBeFalse();
    expect(service.isNumberFibonacci(15)).toBeFalse();
    expect(service.isNumberFibonacci(15)).toBeFalse();
  });

  it('isNumberFibonacci() should return false for the Fibonacci number 1597', () => {
    expect(service.isNumberFibonacci(1597)).toBeFalse();
  });

  it('getNthFibonacci() should return the correct Fibonacci number for the nth value', () => {
    expect(service.getNthFibonacci(1)).toEqual(0);
    expect(service.getNthFibonacci(2)).toEqual(1);
    expect(service.getNthFibonacci(3)).toEqual(1);
    expect(service.getNthFibonacci(4)).toEqual(2);
    expect(service.getNthFibonacci(5)).toEqual(3);
    expect(service.getNthFibonacci(6)).toEqual(5);
    expect(service.getNthFibonacci(7)).toEqual(8);
    expect(service.getNthFibonacci(17)).toEqual(987);
    expect(service.getNthFibonacci(18)).toEqual(1597);
  });
});

import { Router } from '@angular/router';
import { FrequencyStoreService } from './../../services/frequency-store.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-farewell',
  templateUrl: './farewell.component.html',
  styleUrls: ['./farewell.component.scss'],
})
export class FarewellComponent implements OnInit {
  outputText: string;
  constructor(store: FrequencyStoreService, private router: Router) {
    this.outputText = store.getFormatedText();
  }

  ngOnInit(): void {}

  onStart(): void {
    this.router.navigate (['']);
  }
}

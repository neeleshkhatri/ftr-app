import { FibonacciService } from './../../services/fibonacci.service';
import { NumberFrequencyModel } from './../../models/number-frequency.model';
import { FrequencyStoreService } from './../../services/frequency-store.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { FrequencyService } from '../../services/frequency.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-numbers-frequency',
  templateUrl: './numbers-frequency.component.html',
  styleUrls: ['./numbers-frequency.component.scss'],
})
export class NumbersFrequencyComponent implements OnInit, OnDestroy {
  isTimerRunning = true;
  currentCountDownValue = '';
  haltResumeButtonText = this.getHaltResumeButtonText(this.isTimerRunning);
  outputText: string;
  form: FormGroup;
  showToast = false;
  currentFibonacciNumber: number;

  private countDownSubscription: Subscription;

  constructor(
    fb: FormBuilder,
    private router: Router,
    private store: FrequencyStoreService,
    private frequencyService: FrequencyService,
    private fibonacciService: FibonacciService
  ) {
    this.form = fb.group({
      userNumber: ['', Validators.required],
    });
    this.countDownSubscription = frequencyService
      .getCountDown()
      .subscribe(this.subscribeCountDown());
  }

  ngOnInit(): void {
    this.store.clear();
  }

  ngOnDestroy(): void {
    this.countDownSubscription.unsubscribe();
  }

  onHaltResume(): void {
    this.isTimerRunning = !this.isTimerRunning;
    this.haltResumeButtonText = this.getHaltResumeButtonText(
      this.isTimerRunning
    );
    this.frequencyService.toggle$.next(this.isTimerRunning);
  }

  onQuit(): void {
    this.router.navigate(['/farewell']);
  }

  onSubmit(): void {
    const userNumber = this.form.controls.userNumber.value;
    if (this.form.valid) {
      this.store.addToStore(userNumber);
      this.notifyIfFibonacciNumber(userNumber);
      this.clearUserNumberField();
    }
  }

  private subscribeCountDown(): { next: any; complete: any } {
    return {
      next: (d) => (this.currentCountDownValue = d),
      complete: () => {
        this.outputText = this.store.getFormatedText();
        this.countDownSubscription.unsubscribe();
        this.countDownSubscription = this.frequencyService
          .getCountDown()
          .subscribe(this.subscribeCountDown());
      },
    };
  }

  private clearUserNumberField(): void {
    (this.form.controls.userNumber as FormControl).setValue('');
  }

  private getHaltResumeButtonText(isTimerRunning: boolean): string {
    return isTimerRunning ? 'Halt' : 'Resume';
  }

  private notifyIfFibonacciNumber(value: number): void {
    if (this.fibonacciService.isNumberFibonacci(value)) {
      this.showToast = true;
      this.currentFibonacciNumber = value;
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputFrequencyComponent } from './output-frequency.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FrequencyService } from '../../services/frequency.service';
import { By } from '@angular/platform-browser';
export const ButtonClickEvents = {
  left:  { button: 0 },
  right: { button: 2 }
};

describe('OutputFrequencyComponent', () => {
  let component: OutputFrequencyComponent;
  let fixture: ComponentFixture<OutputFrequencyComponent>;
  const frequencyService = jasmine.createSpyObj('FrequencyService', ['setFrequency']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[RouterTestingModule, FormsModule, ReactiveFormsModule],
      providers: [FormBuilder,
        { provide: FrequencyService, useValue: frequencyService }
      ],
      declarations: [ OutputFrequencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputFrequencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call frequencyService.setFrequency', () => {
    const form = fixture.debugElement.query(By.css('form'));
    form.triggerEventHandler('submit', {});
    fixture.detectChanges();
    expect(frequencyService.setFrequency).toHaveBeenCalled();
  });
});

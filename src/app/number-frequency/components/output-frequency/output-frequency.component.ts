import { FrequencyService } from '../../services/frequency.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-output-frequency',
  templateUrl: './output-frequency.component.html',
  styleUrls: ['./output-frequency.component.scss'],
})
export class OutputFrequencyComponent implements OnInit {
  frequencyOutputForm: FormGroup;

  constructor(
    private router: Router,
    fb: FormBuilder,
    private frequencyService: FrequencyService
  ) {
    this.frequencyOutputForm = fb.group({
      frequency: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  public onSave(): void {
    const frequecyValue = this.frequencyOutputForm?.controls?.frequency?.value;
    this.frequencyService.setFrequency(frequecyValue);
    this.router.navigate(['/numbersfrequency']);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OutputFrequencyComponent } from './number-frequency/components/output-frequency/output-frequency.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NumbersFrequencyComponent } from './number-frequency/components/numbers-frequency/numbers-frequency.component';
import { FarewellComponent } from './number-frequency/components/farewell/farewell.component';

@NgModule({
  declarations: [
    AppComponent,
    OutputFrequencyComponent,
    NumbersFrequencyComponent,
    FarewellComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
